workspace "physics"
    configurations { "Release", "Debug" }


    -- LIBRAIRY
    project "physics"
        kind "StaticLib"
        language "C++"

        targetdir "bin/physics/%{cfg.buildcfg}"
        objdir "obj/physics/%{cfg.buildcfg}"

        files { "src/**.hpp", "src/**.cpp" }

        filter "configurations:Release"
            defines { "NDEBUG" }
            optimize "On"

        filter "configurations:Debug"
            defines { "DEBUG" }
            symbols "On"


    -- TESTS
    project "tests"
        kind "ConsoleApp"
        language "C++"

        targetdir "bin/tests/%{cfg.buildcfg}"
        objdir "obj/tests/%{cfg.buildcfg}"
        location "tests"

        includedirs { "src" }
        links { "physics" }

        files { "tests/**.hpp", "tests/**.cpp" }

        filter "configurations:Release"
            defines { "NDEBUG" }
            optimize "On"

        filter "configurations:Debug"
            defines { "DEBUG" }
            symbols "On"


    -- EXAMPLES

    function generate_example_project(dir_path)
        -- Double check that the path is really a directory
        if os.isdir(dir_path) then

            -- Find the name and path of the example folders
            example_folder = "examples/" .. path.getname(dir_path)
            example_name = "example." .. path.getname(dir_path)
            
            -- Add the project
            project(example_name)
                kind "ConsoleApp"
                language "C++"

                targetdir("bin/" .. example_name .. "/%{cfg.buildcfg}")
                objdir("obj/" .. example_name .. "/%{cfg.buildcfg}")
                location(example_folder)

                -- Link to the librairy and add it's folder to the include directories
                includedirs { "src", "inc/examples" }
                links {
                    "physics",
                    -- "lib/examples/raylib_windows",
                    -- "gdi32",
                    -- "opengl32",
                    -- "winmm"
                    "lib/examples/raylib_linux",
                    "GL",
                    "m",
                    "pthread",
                    "dl",
                    "rt",
                    "X11"
                }

                files { example_folder .. "/**.hpp", example_folder .. "/**.cpp" }

                filter "configurations:Release"
                    defines { "NDEBUG" }
                    optimize "On"

                filter "configurations:Debug"
                    defines { "DEBUG" }
                    symbols "On"
        end
    end

    example_dirs = os.matchdirs("examples/*")
    table.foreachi(example_dirs, generate_example_project)