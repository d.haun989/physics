# [Unfinished] A 2D physics engine in C++

<img src="https://gitlab.com/yuesubi/physics/-/raw/main/res/icon.png" width="128px">

## Run examples

### Linux
* Make sure that [raylib](https://raylib.com/),
  [make](https://www.gnu.org/software/make/) and
  [premake](https://premake.github.io/) are available on your system.
* Execute the following commands in a terminal. Replace `__example_name__` with
  the name of a folder in `examples/`.
```bash
premake5 gmake2
make
exec ./bin/example.__example_name__/Release/example.__example_name__
```

### Windows
* Make sure that [raylib](https://raylib.com/),
  [make](https://www.gnu.org/software/make/) (maybe best with
  [mingw64](https://www.mingw-w64.org/) as it also includes **g++**) and
  [premake](https://premake.github.io/) are available on your system from the
  command line. You could also generate Visual Studio project files with
  premake, but this approch has not been tested for this project.
* Execute the following commands in a terminal. Replace `__example_name__` with
  the name of a folder in `examples\`.
```cmd
premake5 gmake2
make
start .\bin\example.__example_name__\Release\example.__example_name__.exe
```

## Project structure
```txt
+-------------+
|    world    |
+-------------+
|  rigidbody  |
+-------------+
|  collider   |
+------+------+
| math | mass |
+------+------+
```

## Project progression

### Backlog Items
* 

### To do
* Add different kinds of AddForce methods, that use or not the mass.

### In Progress
* Add collisions

### Testing
* 

### Done
* 
