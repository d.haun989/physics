#pragma once


namespace pcs {
	struct Mass
	{
		float volumic;
		float value;
		float inv;

		inline Mass() = default;
	};
}