#include "world.hpp"


namespace pcs {

	RigidBody *World::RigidBody_Ptr::operator->(void)
	{
		return &containing_world.m_rigid_bodies[rigid_body_index];
	}

	RigidBody& World::RigidBody_Ptr::operator*(void)
	{
		return containing_world.m_rigid_bodies[rigid_body_index];
	}

	World::RigidBody_Ptr::RigidBody_Ptr(World& p_containing_world,
		std::size_t p_rigid_body_index)
		: containing_world(p_containing_world),
		rigid_body_index(p_rigid_body_index)
	{
	}


	World::World()
		: m_Gravity(math::Vec2::Zero()),
		m_rigid_bodies(),
		m_TimeStep(0.01f)
	{
	}

	World::World(math::Vec2 gravity, float timeStep)
		: m_Gravity(gravity),
		m_rigid_bodies(),
		m_TimeStep(timeStep)
	{
	}

	World::~World()
	{
	}

	void World::SetGravity(math::Vec2 newGravity)
	{
		m_Gravity = newGravity;
	}

	math::Vec2 World::GetGravity() const
	{
		return m_Gravity;
	}

	void World::SetStep(float newTimeStep)
	{
		m_TimeStep = newTimeStep;
	}

	float World::GetStep() const
	{
		return m_TimeStep;
	}

	World::RigidBody_Ptr World::CreateRigidBody(
		const RigidBody_Blueprint& blueprint)
	{
		m_rigid_bodies.push_back(RigidBody(blueprint));
		return RigidBody_Ptr(*this, m_rigid_bodies.size() - 1);
	}

	void World::Step()
	{
		for (RigidBody& rb : m_rigid_bodies) {
			rb.AddForce(m_Gravity * m_TimeStep * rb.GetMass().value);
			rb.position += rb.GetVelocity() * m_TimeStep;
		}
	}
}