#include "aabb_collider.hpp"


namespace pcs {
	AABBCollider::AABBCollider(
			float p_width,
			float p_height,
			math::Vec2 p_local_position,
			float p_local_rotation
		)
		: width(p_width), height(p_height)
	{
		colliderType = AABB_COLLIDER;

		local_position = p_local_position;
		local_rotation = p_local_rotation;
	}

	float AABBCollider::GetMass(float volumicMass) const
	{
		return width * height * volumicMass;
	}
}