#include "circle_collider.hpp"

#include <math.h>


namespace pcs {
	
	CircleCollider::CircleCollider(
		float p_radius,
		math::Vec2 p_local_position,
		float p_local_rotation
		)
		: radius(p_radius)
	{
		colliderType = CIRCLE_COLLIDER;

		local_position = p_local_position;
		local_rotation = p_local_rotation;
	}

	float CircleCollider::GetMass(float volumicMass) const
	{
		return radius * radius * M_PI;
	}
}