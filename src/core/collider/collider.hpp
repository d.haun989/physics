#pragma once

#include "../math/vector2.hpp"


namespace pcs {
	class Collider
	{
	public:
		enum ColliderType {
			AABB_COLLIDER,
			CIRCLE_COLLIDER
		};
	
	public:
		math::Vec2 local_position;
		float local_rotation;

	public:
		virtual float GetMass(float volumicMass) const = 0;
	
	protected:
		ColliderType colliderType;
	};
}