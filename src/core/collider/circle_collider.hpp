#pragma once

#include "collider.hpp"


namespace pcs {
	class CircleCollider : public Collider
	{
	public:
		// The radius of the collider
		float radius;

	public:
		CircleCollider(
			float radius,
			math::Vec2 local_position = math::Vec2::Zero(),
			float local_rotation = 0.0f
		);

		virtual float GetMass(float volumicMass) const override;
	};
}