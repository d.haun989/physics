#pragma once

#include "collider.hpp"


namespace pcs {
	class AABBCollider : public Collider
	{
	public:
		float width;
		float height;
	
	public:
		AABBCollider(
			float width,
			float height,
			math::Vec2 local_position = math::Vec2::Zero(),
			float local_rotation = 0.0f
		);

		virtual float GetMass(float volumicMass) const override;
	};
}