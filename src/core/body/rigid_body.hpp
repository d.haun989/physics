#pragma once

#include <vector>

#include "../collider/collider.hpp"
#include "../mass/mass.hpp"
#include "../math/vector2.hpp"
#include "rigid_body_blueprint.hpp"


namespace pcs {
	class RigidBody
	{

	public:
		math::Vec2 position;
		float rotation;

	public:
		RigidBody(const RigidBody_Blueprint& blueprint);

		void AddForce(math::Vec2 force);
		void AttachCollider(Collider *collider);

		math::Vec2 GetVelocity(void) const;
		Mass GetMass(void) const;

	private:
		math::Vec2 m_velocity;
		float m_angular_velocity;

		Mass m_mass;
		
		bool m_is_static;
		bool m_is_trigger;

    	CollisionChecking m_collision_checking;
		std::vector<Collider *> m_colliders;

	private:
		void CalculateMass();
	};
}