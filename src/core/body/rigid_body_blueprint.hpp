#pragma once

#include "../math/vector2.hpp"
#include "collision_checking.hpp"


namespace pcs {
	struct RigidBody_Blueprint
	{
	public:
		math::Vec2 position;
		float rotation;

		float volumic_mass;
		bool is_static;

    	bool is_trigger;
    	CollisionChecking collision_checking;
	
	public:
		inline RigidBody_Blueprint() = default;
	};
}