#include "rigid_body.hpp"


namespace pcs {

	RigidBody::RigidBody(const RigidBody_Blueprint& blueprint)
	{
		position = blueprint.position;
		rotation = blueprint.rotation;

		m_velocity = math::Vec2::Zero();
		m_angular_velocity = 0.0f;

		m_is_static = blueprint.is_static;
		m_is_trigger = blueprint.is_trigger;

		m_collision_checking = blueprint.collision_checking;

		m_mass.volumic = blueprint.volumic_mass;
		CalculateMass();
	}

	void RigidBody::AddForce(math::Vec2 force)
	{
		m_velocity += force;
	}

	void RigidBody::AttachCollider(Collider *collider)
	{
		m_colliders.push_back(collider);
		CalculateMass();
	}

	math::Vec2 RigidBody::GetVelocity(void) const
	{
		return m_velocity;
	}

	Mass RigidBody::GetMass(void) const
	{
		return m_mass;
	}

	void RigidBody::CalculateMass()
	{
		m_mass.value = 0.0f;
		for (const Collider *collider : m_colliders)
			m_mass.value += collider->GetMass(m_mass.volumic);
		
		if (m_mass.value != 0.0f) {
			m_mass.inv = 1.0f / m_mass.value;
		} else {
			m_mass.inv = 0.0f;
		}
	}
}