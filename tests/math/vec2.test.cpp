#include "../catch_amalgamated.hpp"

#include "physics.hpp"


TEST_CASE( "Test vector methods", "[math/vector]" )
{
    using namespace pcs::math;

	SECTION("Length()") {
		REQUIRE(Vec2(0.0f, 0.0f).Length() == 0.0f);
    	REQUIRE(Vec2(1.0f, 0.0f).Length() == 1.0f);
    	REQUIRE(Vec2(-3.0f, 4.0f).Length() == 5.0f);
    	REQUIRE(Vec2(0.0f, -10.0f).Length() == 10.0f);
	}

	SECTION("SqrdLength()") {
		REQUIRE(Vec2(0.0f, 0.0f).SqrdLength() == 0.0f);
    	REQUIRE(Vec2(1.0f, 0.0f).SqrdLength() == 1.0f);
    	REQUIRE(Vec2(-3.0f, 4.0f).SqrdLength() == 25.0f);
    	REQUIRE(Vec2(0.0f, -10.0f).SqrdLength() == 100.0f);
	}

	// SECTION("operator==()") {
	// 	REQUIRE(Vec2(0.0f, 0.0f) == Vec2::Zero());
    // 	REQUIRE(Vec2(0.0f, 1.0f) == Vec2(1.0f, 0.0f));
    // 	REQUIRE(Vec2(0.0f, 10.0f) == Vec2(0.0f, -10.0f));
    // 	REQUIRE(Vec2(-0.0f, 50.0f) == Vec2(0.0f, 50.0f));
	// }
}